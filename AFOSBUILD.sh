rm -rf /opt/ANDRAX/sdnpwn

source /opt/ANDRAX/PYENV/python3-1/bin/activate

/opt/ANDRAX/PYENV/python3-1/bin/pip3 install wheel

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install wheel... PASS!"
else
  # houston we have a problem
  exit 1
fi

/opt/ANDRAX/PYENV/python3-1/bin/pip3 install netifaces scipy tabulate scapy websocket-client

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir lib
cd lib
git clone https://github.com/smythtech/python-openflow-legacy
cd python-openflow-legacy
chmod +x setup.py
/opt/ANDRAX/PYENV/python3-1/bin/python3 setup.py install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Setup install python-openflow-legacy... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd ../..

rm -rf lib/

cp -Rf $(pwd) /opt/ANDRAX/sdnpwn

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
